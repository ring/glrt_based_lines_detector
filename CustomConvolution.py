
"""
.. module:: Narrow Rivers Extraction 
    :synopsis: Modified convolution using FFT that allows to perform the Image FFT only once for multiple convolutions.

.. moduleauthor:: Nicolas Gasnier - CS GROUP - CNES - Télécom Paris

..
   This file is part of the river detection demonstrator for N.Gasnier et al. « Narrow River Extraction from SAR Images
 Using Exogenous Information submitted to IEEE JSTARS »
   Copyright (C) 2020 Centre National d’Etudes Spatiales, CS GROUP, Télécom Paris
   This software is released under open source license GPL v.3 and is distributed WITHOUT ANY WARRANTY, read LICENSE.txt for further details.

"""


# Code adapted from https://github.com/scipy/scipy/blob/v0.16.1/scipy/signal/signaltools.py


from numpy.fft import rfftn, irfftn
from numpy import (allclose, angle, arange, argsort, array, asarray,
                   atleast_1d, atleast_2d, cast, dot, exp, expand_dims,
                   iscomplexobj, isscalar, mean, ndarray, newaxis, ones, pi,
                   poly, polyadd, polyder, polydiv, polymul, polysub, polyval,
                   prod, product, r_, ravel, real_if_close, reshape,
                   roots, sort, sum, take, transpose, unique, where, zeros,
                   zeros_like)
import numpy as np
from scipy.fftpack import (fft, ifft, ifftshift, fft2, ifft2, fftn,
                           ifftn, fftfreq)
#from scipy._lib._version import NumpyVersion
import threading


def centered(arr, newsize):
    # Return the center newsize portion of the array.
    newsize = asarray(newsize)
    currsize = array(arr.shape)
    startind = (currsize - newsize) // 2
    endind = startind + newsize
    myslice = [slice(startind[k], endind[k]) for k in range(len(endind))]
    return arr[tuple(myslice)]


def next_regular(target):
    """
    Find the next regular number greater than or equal to target.
    Regular numbers are composites of the prime factors 2, 3, and 5.
    Also known as 5-smooth numbers or Hamming numbers, these are the optimal
    size for inputs to FFTPACK.
    Target must be a positive integer.
    """
    if target <= 6:
        return target

    # Quickly check if it's already a power of 2
    if not (target & (target - 1)):
        return target

    match = float('inf')  # Anything found will be smaller
    p5 = 1
    while p5 < target:
        p35 = p5
        while p35 < target:
            # Ceiling integer division, avoiding conversion to float
            # (quotient = ceil(target / p35))
            quotient = -(-target // p35)

            # Quickly find next power of 2 >= quotient
            try:
                p2 = 2 ** ((quotient - 1).bit_length())
            except AttributeError:
                # Fallback for Python <2.7
                p2 = 2 ** (len(bin(quotient - 1)) - 2)

            N = p2 * p35
            if N == target:
                return N
            elif N < match:
                match = N
            p35 *= 3
            if p35 == target:
                return p35
        if p35 < match:
            match = p35
        p5 *= 5
        if p5 == target:
            return p5
    if p5 < match:
        match = p5
    return match


rfft_mt_safe = True#(NumpyVersion(np.__version__) >= '1.9.0.dev-e24486e')
rfft_lock = threading.Lock()


def precompute_fft(image, patch_size):
    s1 = array([patch_size, patch_size])
    s2 = array(image.shape)
    shape = s1 + s2 - 1
    fshape = [next_regular(int(d)) for d in shape]
    return rfftn(image, fshape)


def fftconvolveCustom(fftin1, in2, shapeImage):
    """Convolve two N-dimensional arrays using FFT.
    Convolve `in1` and `in2` using the fast Fourier transform method, with
    the output size determined by the `mode` argument.
    This is generally much faster than `convolve` for large arrays (n > ~500),
    but can be slower when only a few output values are needed, and can only
    output float arrays (int or object array inputs will be cast to float).
    Parameters
    ----------
    in1 : array_like
        The FFT of in1. First input.
    in2 : array_like
        Second input. Should have the same number of dimensions as `in1`;
        if sizes of `in1` and `in2` are not equal then `in1` has to be the
        larger array.
    shapeImage : int tuple
                the shape of in1
    Returns
    -------
    out : array
        An 2-dimensional array containing the discrete linear
        convolution of `in1` with `in2`.
    Examples"""

    # in1 = asarray(in1)
    # in2 = asarray(in2)

    s1 = array(shapeImage)
    s2 = array(in2.shape)
    complex_result = False
    shape = s1 + s2 - 1

    # Speed up FFT by padding to optimal size for FFTPACK
    fshape = [next_regular(int(d)) for d in shape]
    fslice = tuple([slice(0, int(sz)) for sz in shape])
    # Pre-1.9 NumPy FFT routines are not threadsafe.  For older NumPys, make
    # sure we only call rfftn/irfftn from one thread at a time.
    if (rfft_mt_safe or _rfft_lock.acquire(False)):
        try:
            ret = irfftn(fftin1 *
                         rfftn(in2, fshape), fshape)[fslice].copy()
        finally:
            if not rfft_mt_safe:
                rfft_lock.release()
    else:
        # If we're here, it's either because we need a complex result, or we
        # failed to acquire _rfft_lock (meaning rfftn isn't threadsafe and
        # is already in use by another thread).  In either case, use the
        # (threadsafe but slower) SciPy complex-FFT routines instead.
        ret = ifftn(fftin1 * fftn(in2, fshape))[fslice].copy()
        if not complex_result:
            ret = ret.real

    return centered(ret, s1)
