"""
.. module:: Narrow Rivers Extraction
    :synopsis: Main file:  loads the images, runs the computation and displays the results.

.. moduleauthor:: Nicolas Gasnier - CS GROUP - CNES - Télécom Paris

..
   This file was released as a part of the river detection demonstrator for N.Gasnier et al. « Narrow River Extraction from SAR Images
 Using Exogenous Information submitted to IEEE JSTARS »
   Copyright (C) 2020 Centre National d’Etudes Spatiales, CS GROUP, Télécom Paris
   This software is released under open source license GPL v.3 and is distributed WITHOUT ANY WARRANTY, read LICENSE.txt for further details.

"""


import numpy as np
from utils import *
import matplotlib.pyplot as plt
from line_detector import *



image_sar = loadRedonGRD()

lines_detector_response = compute_lines_detection(image_sar,scalemin=1, scalemax = 4)


plt.figure()
plt.imshow(return_LDR(lines_detector_response, lines_detector_response * 0), aspect='equal', interpolation = None)
plt.axis('off')
plt.title("Linear structure detector response")
plt.show(block = False)


plt.figure()
plt.imshow(image_sar,vmax=display_threshold(image_sar),cmap = 'gray', aspect='equal', interpolation = None)
plt.axis('off')
plt.title("Image and centerline " )
plt.show(block = False)





plt.show()

