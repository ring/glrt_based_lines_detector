# GLRT Based line detector

Python implementation of the GLRT based linear features detector proposed by N.Gasnier, L.Denis, and F.Tupin in Generalized Likelihood Ratio Tests for Linear Structure Detection in SAR Images (EUSAR 2021).
