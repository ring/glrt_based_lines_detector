"""
.. module:: Narrow Rivers Extraction
    :synopsis: Loads useful functions

.. moduleauthor:: Nicolas Gasnier - CS GROUP - CNES - Télécom Paris

..
   This file is part of the river detection demonstrator for N.Gasnier et al. « Narrow River Extraction from SAR Images
 Using Exogenous Information submitted to IEEE JSTARS »
   Copyright (C) 2020 Centre National d’Etudes Spatiales, CS GROUP, Télécom Paris
   This software is released under open source license GPL v.3 and is distributed WITHOUT ANY WARRANTY, read LICENSE.txt for further details.

"""
import matplotlib.pyplot as plt
import numpy as np
from skimage.morphology import binary_dilation
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.matlab.mio import loadmat
from colorsys import hsv_to_rgb

from skimage.morphology import disk
from skimage.morphology import square
from skimage.morphology import binary_closing
from skimage.morphology import binary_opening
import pickle as pkl
import matplotlib.pyplot as plt
import matplotlib.cm


def display_threshold(image, valspe = 3):
    '''
    :param image: The SAR image
    :param valspe:
    :return: The max value for the display of the image
    '''
    xmean = np.mean(image)
    xstd = np.std(image)
    xseuil = xmean + valspe * xstd
    return xseuil


def draw_centerline(I, centerline):
    plt.figure()
    centerlined = binary_dilation(centerline)
    w,h = I.shape
    Im = I/np.percentile(I,95)
    Im[Im>1] = 1
    RGB = np.zeros((w,h,3))
    RGB[:,:,0] = np.where(centerlined, np.ones((w,h)) , Im)
    RGB[:,:,1] = np.where(centerlined, np.zeros((w,h)) , Im)
    RGB[:,:,2] = np.where(centerlined, np.zeros((w,h)) , Im)
    plt.imshow(RGB)
    plt.title('Centerline')
    plt.show(block = False)

def color_display(SAR_image, binary, channel=0):
    '''

    :param SAR_image: The SAR image
    :param binary: The binary image to display
    :param channel: The color wanted for the binary image: 0 => red, 1=>green, 2=>blue
    :return: a rgb image with the binary image on the selected color on the SAR image
    '''
    vmax = display_threshold(SAR_image)
    image_normalized = SAR_image.copy()
    SAR_image[SAR_image>vmax] = vmax
    image_normalized = image_normalized/vmax
    rgb = np.dstack((image_normalized,image_normalized,image_normalized))
    rgb[binary>0, 0] = 1.0 * (channel == 0)
    rgb[binary>0, 1] = 1.0 * (channel == 1)
    rgb[binary>0, 2] = 1.0 * (channel == 2)
    if channel == 2:
        rgb[binary>0, 1] = 0.7
        rgb[binary>0, 2] = 1.0
    return rgb


def color_display_error(SAR_image, detected,truth):
    '''

    :param SAR_image: The SAR image
    :param binary: The binary image to display
    :param channel: The color wanted for the binary image: 0 => red, 1=>green, 2=>blue
    :return: a rgb image with the binary image on the selected color on the SAR image
    '''
    vmax = display_threshold(SAR_image)
    image_normalized = SAR_image.copy()
    SAR_image[SAR_image>vmax] = vmax
    image_normalized = image_normalized/vmax
    rgb = np.dstack((image_normalized,image_normalized,image_normalized))
    truth = (truth==1)  + (truth==101) + (truth==103) + (truth==102) + (truth==99) + (truth==106)
    TP = np.logical_and(truth,detected)
    FN = np.logical_and(truth,np.logical_not(detected))
    FP = np.logical_and(detected,np.logical_not(truth))
    covered = np.logical_or(truth, detected)
    rgb[covered > 0, 0] = 0.0
    rgb[covered > 0, 1] = 0.0
    rgb[covered > 0, 2] = 0.0
    rgb[TP > 0, 2] = 1.0
    rgb[TP > 0, 1] = 0.7
    rgb[FN > 0, 0] = 1.0
    rgb[FP > 0, 1] = 1.0
    rgb[FP > 0, 0] = 1.0
    return rgb



def color_display_error_uncertain(SAR_image, detected_water_mask,truth):
    '''
    :param SAR_image: The SAR image
    :param binary: The binary image to display
    :param channel: The color wanted for the binary image: 0 => red, 1=>green, 2=>blue
    :return: a rgb image with the binary image on the selected color on the SAR image
    '''
    vmax = display_threshold(SAR_image)
    image_normalized = SAR_image.copy()
    SAR_image[SAR_image>vmax] = vmax
    image_normalized = image_normalized/vmax
    rgb = np.dstack((image_normalized,image_normalized,image_normalized))
    water_truth = truth[:,:,0] > 0
    uncertain = truth[:,:,1] > 0


    TP =  detected_water_mask * (water_truth + uncertain)
    FN = water_truth * np.logical_not(detected_water_mask) * np.logical_not(uncertain)
    FP = detected_water_mask * np.logical_not(water_truth) * np.logical_not(uncertain)
    TN = np.logical_not(detected_water_mask) * (np.logical_not(water_truth) + uncertain)
    covered = TP + FN + FP
    rgb[covered > 0, 0] = 0.0
    rgb[covered > 0, 1] = 0.0
    rgb[covered > 0, 2] = 0.0
    rgb[TP > 0, 2] = 1.0
    rgb[TP > 0, 1] = 0.7
    rgb[FN > 0, 0] = 1.0
    rgb[FP > 0, 1] = 1.0
    rgb[FP > 0, 0] = 1.0
    return rgb




def ComputeDetectionError(detected_water_mask, truth):
    """
    Computes quality criterion for the water detection
    """
    nb_pixel = detected_water_mask.size

    water_truth = truth[:,:,0] > 0
    uncertain = truth[:,:,1] > 0
    TP =  detected_water_mask * (water_truth + uncertain)
    FN = water_truth * np.logical_not(detected_water_mask) * np.logical_not(uncertain)
    FP = detected_water_mask * np.logical_not(water_truth) * np.logical_not(uncertain)
    TN = np.logical_not(detected_water_mask) * (np.logical_not(water_truth) + uncertain)
    accuracy = np.sum(np.logical_or(TP,TN)==1)/np.size(detected_water_mask)

    precision = np.sum(TP==1)/np.sum(np.logical_or(TP,FP)==1)
    recall = np.sum(TP==1) / np.sum(np.logical_or(TP,FN)==1)
    Fscore = 2*(precision*recall)/(precision+recall)
    ER = np.sum(np.logical_or(FP,FN)==1)/np.sum(np.logical_or(TP,FN)==1)
    TP = np.sum(TP)/nb_pixel
    TN = np.sum(TN)/nb_pixel
    FP = np.sum(FP)/nb_pixel
    FN = np.sum(FN)/nb_pixel
    precision2 = TP/(TP+FP)
    recall2 = TP/(TP+FN)
    FPR2 = FP/(FP+TN)

    MCC = ((TP*TN)-(FP*FN))/np.sqrt((TP+FN)*(FP+TN)*(TP+FP)*(TN+FN))
    FPR = FP/(FP+TN)
    return accuracy, precision, recall, Fscore, ER, MCC, FPR

def DisplayDetectionError(detected_water_mask, truth):
    accuracy, precision, recall, Fscore, ER, MCC, FPR = ComputeDetectionError(detected_water_mask, truth)

    print("Pre : {:.2%}, Rec : {:.2%}, FPR : {:.2%}, F-Score : {:.2%}, ER : {:.2%}, MCC : {:.2%}".format(precision, recall, FPR, Fscore, ER, MCC))
    print(" {:.2%} & {:.2%} &  {:.2%} & {:.2%} & {:.2%} & {:.2%}".format(precision, recall, FPR, Fscore, ER, MCC).replace('%', ''))



def loadDesMoinesGRD():
    file = "./Data/DesMoinesRacoonRiver_vh_20180802.tif"
    return plt.imread(file).astype(np.float)


def loadGambieGRD():
    file = "./Data/Gambie1_vh_20180715.tif"
    return plt.imread(file).astype(np.float)

def loadGambieTruth():
    file = "./Data/Gambie1_vh_20180715Truth.png"
    truth = plt.imread(file)
    return truth

def loadAngersGRD():
    file = "./Data/Angers_vv_20191202.tif"
    return plt.imread(file).astype(np.float)

def loadAngersTruth():
    file = "./Data/Angers_vv_20191202Truth.png"
    truth = plt.imread(file)
    return truth

def loadToulouseGRD():
    file = "./Data/ToulouseNord_vv_20200209.tif"
    return plt.imread(file).astype(np.float)

def loadSagarGRD():
    file = "./Data/SagarRiver_vh_20180622.tif"
    return plt.imread(file).astype(np.float)


def loadSagarTruth():
    file = "./Data/SagarRiver_vh_20180622Truth.png"
    truth = plt.imread(file)
    return truth

def loadDesMoinesTruth():
    file = "./Data/DesMoinesRacoonRiver_vh_20180802Truth.png"
    truth = plt.imread(file)
    return truth

def loadToulouseTruth():
    file = "./Data/ToulouseNord_vv_20200209Truth.png"
    truth = plt.imread(file)
    return truth

def loadRedonTruth():
    file = "./Data/Redon_vh_20180704Truth.png"
    truth = plt.imread(file)
    return truth

def loadCanalCamargueTruth():
    file = "./Data/verite_Camargue.png"
    truth = plt.imread(file)
    return truth

def loadCamargueTruth():
    file = "./Data/verite_Camargue.png"
    truth = plt.imread(file)
    return truth

def loadRedonGRD():
    file = "./Data/Redon_vh_20180704.tif"
    return plt.imread(file).astype(np.float)

def loadArataiTruth():
    file = "./Data/Aratai_River_vh_20170414Truth.png"
    truth = plt.imread(file)
    return truth

def loadLidarUSTruth():
    file = "./Data/LidarUS_truth.png"
    truth = plt.imread(file)
    return truth

def loadArataiGRD():
    file = "./Data/Aratai_River_vh_20171011.tif"
    return plt.imread(file).astype(np.float)

def add_nodes_display(coords_a_priori):
    pairs_number = len(coords_a_priori)
    for pair_nodes in coords_a_priori:
        color = hsv_to_rgb(pair_nodes[0] / pairs_number, 1, 1)
        plt.scatter([pair_nodes[2], pair_nodes[4]], [pair_nodes[1], pair_nodes[3]], c=color, marker ="+", s=50)
    return 1






def crop_image_from_river_a_priori(image, listy, listx, marge):
    w,h = image.shape
    print('shape',image.shape)
    xmin = max(min(listx)-marge, 0)
    xmax = min(max(listx)+marge, w-1)
    ymin = max(min(listy)-marge, 0)
    ymax = min(max(listy)+marge, h-1)
    image = image[xmin:xmax, ymin:ymax]
    listxr = [elem - xmin for elem in listx]
    listyr = [elem - ymin for elem in listy]
    print(xmin, xmax, ymin, ymax)
    print(image.shape)
    return image, listxr, listyr




def centerline_to_list(listxr, listyr, distance_points):
    liste_coords = []
    number = len(listxr)//distance_points
    print('number',number, len(listxr))

    for pair_rank in range(number-1):
        print(pair_rank, pair_rank * distance_points,pair_rank * distance_points + 2 * distance_points)
        y0 = listyr[pair_rank * distance_points]
        y1 = listyr[pair_rank * distance_points + 2 * distance_points -1]
        x0 = listxr[pair_rank * distance_points]
        x1 = listxr[pair_rank * distance_points + 2 * distance_points -1]
        paire = [pair_rank + 1, y0, x0, y1, x1]
        liste_coords.append(paire)
    pair_rank = number-1
    y0 = listyr[pair_rank * distance_points]
    y1 = listyr[-1]
    x0 = listxr[pair_rank * distance_points]
    x1 = listxr[-1]
    paire = [pair_rank + 1, y0, x0, y1, x1]
    liste_coords.append(paire)
    return liste_coords


def liste_one_river_from_database_SWOT(river_ref):
    folder = '/data3/ngasnier/DataChaineRivieresFines/'
    refloc = pkl.load( open(folder+ "pickle_refloc.pkl", "rb" ) )
    liste_riviere = pkl.load(open(folder+'liste_rivieres_camargue.pkl','rb')) # Chargement de la loste des points passant par la centerlines pour les rivieres de la BDD pour la Camargue   i
    liste_complete = ProjectRiver(liste_riviere[river_ref],refloc)
    return liste_complete

def ProjectRiver(riviere, refloc):
    """
    Computes the coordinates of the river on the SWOT image according to the given refloc
    """
    latitude, longitude, height = refloc
    latitude = latitude * 180.0 / np.pi
    longitude = longitude * 180.0 / np.pi
    arraycoord = np.dstack((latitude,longitude))
    listex = []
    listey = []
    for point in riviere:
        lat = point[0]
        lon = point[1]
        ooo = (arraycoord[:,:,0] - lat) *(arraycoord[:,:,0] - lat)  + (arraycoord[:,:,1] - lon)*(arraycoord[:,:,1] - lon)

        idx = np.unravel_index(np.argmin(ooo), ooo.shape)
        print(idx)
        listex.append(np.round(idx[0]))
        listey.append(np.round(idx[1]))
    return listex, listey


def draw_centerline_on_LDR(I, centerline):
    plt.figure()
    centerlined = binary_dilation(centerline)
    centerlined = centerline
    w,h = I.shape
    Im = I/np.amax(I)
    Im[Im>1] = 1
    RGB = np.zeros((w,h,3))
    RGB[:,:,0] = np.where(centerlined, np.ones((w,h)) , Im)
    RGB[:,:,1] = np.where(centerlined, np.zeros((w,h)) , Im)
    RGB[:,:,2] = np.where(centerlined, np.zeros((w,h)) , Im)
    plt.imshow(RGB)
    plt.title('Centerline')
    plt.show(block = False)
    plt.figure()
    Im = I/np.percentile(I,95)
    Im[Im>1] = 1
    Im = Im
    RGB = np.zeros((w,h,3))
    RGB[:,:,0] = np.where(centerlined, np.ones((w,h)) , Im)
    RGB[:,:,1] = np.where(centerlined, np.zeros((w,h)) , Im)
    RGB[:,:,2] = np.where(centerlined, np.zeros((w,h)) , Im)
    plt.imshow(RGB)
    plt.title('Centerline')
    plt.show(block = False)



def return_LDR(I, centerline):
    centerlined = binary_dilation(centerline)
    #centerlined = centerline
    w,h = I.shape
    Im = I/np.amax(I)
    Im[Im>1] = 1
    RGB = np.zeros((w,h,3))
    RGB[:,:,0] = np.where(centerlined, np.ones((w,h)) , Im)
    RGB[:,:,1] = np.where(centerlined, np.zeros((w,h)) , Im)
    RGB[:,:,2] = np.where(centerlined, np.zeros((w,h)) , Im)
    Im = I/np.percentile(I,99)
    Im[Im>1] = 1
    Im = Im
    RGB = np.zeros((w,h,3))
    RGB[:,:,0] = np.where(centerlined, np.ones((w,h)) , Im)
    RGB[:,:,1] = np.where(centerlined, np.zeros((w,h)) , Im)
    RGB[:,:,2] = np.where(centerlined, np.zeros((w,h)) , Im)
    return RGB
