"""
.. module:: Narrow Rivers Extraction 
    :synopsis: Performs the linear features detection

    Adapted from a Matlab code written by Loïc Denis - Saint-Etienne University - UMR 5516 CNRS - Télécom Saint-Etienne

.. moduleauthor:: Nicolas Gasnier - CS GROUP - CNES - Télécom Paris

..
   This file is part of the river detection demonstrator for N.Gasnier et al. « Narrow River Extraction from SAR Images
 Using Exogenous Information submitted to IEEE JSTARS »
   Copyright (C) 2020 Centre National d’Etudes Spatiales, CS GROUP, Télécom Paris
   This software is released under open source license GPL v.3 and is distributed WITHOUT ANY WARRANTY, read LICENSE.txt for further details.

"""

import numpy as np
from numpy import exp, log
from scipy.sparse import dok_matrix
from scipy.linalg import pinv
from skimage.transform import resize, rescale
from CustomConvolution import fftconvolveCustom, precompute_fft # Fast convolutions for the same image


def compute_lines_detection(image, scalemin=1, scalemax=4, padding_width=30, **kwargs):
    '''
    Compute the log of the image, pad, resize and call the function that
     computes the line detector response on it.

    :param image: numpy array # the SAR image in amplitude or intensity
    :param scalemin: int # Minimum scale used for computation
    :param scalemax: int # Maximum scale used for computation
    :param padding_width: int # Width of the padding used to prevent artifacts near the edges of the image.
    :param kwargs:
    :return:
    '''
    image_pad = np.pad(np.abs(image), padding_width, mode='symmetric')
    liste_scales = range(scalemin, scalemax + 1)
    I_log = np.log(image_pad+0.001)  # Added to prevent computing log(0)
    w, h = image_pad.shape
    multiscale_results = np.zeros((w, h, len(liste_scales)))
    for scale in liste_scales:
        print("Current scale  :  ", scale)
        log_image_rescaled = rescale(I_log, 1 / scale,mode='reflect',   multichannel=False, anti_aliasing=None )
        multiscale_results[:, :, scale - liste_scales[0]] = resize(
            compute_lines_detection_one_scale(log_image_rescaled,mode='reflect', anti_aliasing=None, **kwargs), (w, h))
    return np.squeeze(np.sum(multiscale_results[padding_width:-padding_width, padding_width:-padding_width, :], 2))


def compute_lines_detection_one_scale(log_image_rescaled, angular_step=3, patch_half_size=4, swot=False, **kwargs):
    '''
 Compute the line detector response for one given scale

    :param log_image_rescaled: the log of the rescalaled image
    :param angular_step: int # Minimum scale used for computation
    :param patch_half_size: int # Maximum scale used for computation
    :param swot: bool True if lines are bright. False if lines are dark.
    :param kwargs:
    :return:
    '''
    patch_size = int(2 * patch_half_size + 1)
    fft_log_image_rescaled = precompute_fft(log_image_rescaled, patch_size) #The FFT of the image is precomputed for fast convolution
    shape_log_image_rescaled = log_image_rescaled.shape
    list_theta = 2 * np.pi * np.arange(0, 180 - angular_step, angular_step) / 360
    profil_ext = int(np.ceil(np.sqrt(2) * (patch_half_size + 1)))
    Cmin = -np.ones_like(log_image_rescaled) * np.inf
    # Compute the sum of the first two terms
    E1 = -np.square(
        fftconvolveCustom(fft_log_image_rescaled, np.ones((patch_size, patch_size)), shape_log_image_rescaled)) / (
                     2 * patch_size ** 2)

    # Compute last two terms
    for theta in list_theta:
        x = np.arange(- patch_half_size, patch_half_size + 1, 1)
        y = np.arange(- patch_half_size, patch_half_size + 1, 1)
        xx, yy = np.meshgrid(x, y)
        k = 0
        V = np.zeros((2 * xx.size))
        M = dok_matrix((xx.size, profil_ext), dtype=np.float32)
        for i in range(xx.shape[0]):
            for j in range(xx.shape[1]):
                p = np.abs(xx[i, j] * np.cos(theta) + yy[i, j] * np.sin(theta))
                M[k, np.floor(p)] = abs(p - np.ceil(p))
                M[k, np.ceil(p)] = 1 - abs(p - np.ceil(p))
                V[2 * k] = abs(p - np.ceil(p))
                V[2 * k + 1] = 1 - abs(p - np.ceil(p))
                k = k + 1
        Md = M.todense()
        P = pinv(Md)  # Moore-Penrose pseudoinverse
        Profiles = np.zeros((log_image_rescaled.shape[0], log_image_rescaled.shape[1], P.shape[0]))
        for i in range(P.shape[0]):
            Profiles[:, :, i] = fftconvolveCustom(fft_log_image_rescaled,
                                                  np.reshape(P[i, :], (patch_size, patch_size), order='C'),
                                                  shape_log_image_rescaled)
        if swot:
            Profiles = np.minimum(np.dstack([Profiles[:, :, 0]] * Md.shape[1]), Profiles)
        else:
            Profiles = np.maximum(np.dstack([Profiles[:, :, 0]] * Md.shape[1]), Profiles)

        E2 = np.zeros_like(E1)
        for i in range(Md.shape[1]):
            E2 = E2 + Profiles[:, :, i] * fftconvolveCustom(fft_log_image_rescaled,
                                                            np.reshape(Md[:, i], (patch_size, patch_size),
                                                                       order='C'), shape_log_image_rescaled)
        u, s, vh = np.linalg.svd(Md, full_matrices=False)
        proj = np.zeros_like(Profiles)

        for i in range(vh.shape[0]):
            proj[:, :, i] = s[i] * np.sum(np.multiply(Profiles, np.broadcast_to(vh[i, :], Profiles.shape)), 2)
        E3 = -0.5 * np.sum(proj ** 2, 2)
        Cmin = np.maximum(Cmin, (E1 + E2 + E3))
    return Cmin
